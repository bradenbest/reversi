#ifndef AI_H
#define AI_H

struct aimovemap {
    char movetext [3];
    int  score       ;
};

typedef char const *(*movetextfn)(int who);

extern char const *ai_addr_no_moves;

char const * ai_cpu_veryeasy_move (int who);
char const * ai_cpu_easy_move     (int who);
char const * ai_cpu_medium_move   (int who);
char const * ai_cpu_hard_move     (int who);
char const * ai_cpu_veryhard_move (int who);

#endif // AI_H



#ifdef AI_C

typedef int (*cmpfn)(void const *a, void const *b);

static struct aimovemap const * get_movelist         (int who);
static size_t                   movelist_len         (struct aimovemap const *movemap);
static struct aimovemap const * movelist_sorted      (struct aimovemap const *movemap, cmpfn callback);
static char const *             ai_cpu_generic_rand3 (int who, cmpfn sorter);

static int cmpfn_movelist_asc  (void const *a, void const *b);
static int cmpfn_movelist_desc (void const *a, void const *b);

#endif // AI_C
