#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "grid.h"
#include "util.h"
#include "draw.h"
#include "coord.h"
#include "io.h"
#include "ai.h"

#define REVERSI_C
#include "reversi.h"

#ifndef VERSION
#define VERSION "<undefined>"
#endif

int
main(void)
{
    puts("Reversi v" VERSION " by Braden Best (c) 2022");
    puts("This software is public domain and is distributed without warranty.");
    srand(time(NULL));
    settings();
    printf("Valid moves are of the form [a-h][1-8]. For example: d3\n");
    gameloop();
    endgamestats();
    return 0;
}

// static

static movetextfn movetextfns[PTYPE_END] = {
    human_move,
    ai_cpu_veryeasy_move,
    ai_cpu_easy_move,
    ai_cpu_medium_move,
    ai_cpu_hard_move,
    ai_cpu_veryhard_move,
};

static settingsfn settings_callbacks[3] = {
    settings_multiplayer,
    settings_singleplayer,
    settings_spectator
};

static struct player players[2];

/* input callbacks
 * who: GSQ_BLACK or GSQ_WHITE
 * return: string [a-h][1-8]
 *
 * these functions take a player ID (GSQ_BLACK or GSQ_WHITE) as input
 * and return a string representing a move such as "d3"
 */

static char const *
human_move(int unused)
{
    (void)(unused);

    return io_getmove();
}

/* player initializer callbacks
 * return: 0 or 1
 *
 * these functions initialize the players based on what game type was
 * selected. The value returned represents which player is black.
 */

static int
settings_multiplayer(void)
{
    player_init(players + 0, GSQ_BLACK, PTYPE_HUMAN, "Player 1 (X)");
    player_init(players + 1, GSQ_WHITE, PTYPE_HUMAN, "Player 2 (O)");
    return 0;
}

static int
settings_singleplayer(void)
{
    int choice;

    puts("Who plays first?");
    puts("  1. Player");
    puts("  2. CPU");
    io_showprompt("Settings/Singleplayer/Whoisfirst");
    choice = clamp(io_getint() - 1, 0, 1);

    if(choice == 0){
        player_init(players + 0, GSQ_BLACK, PTYPE_HUMAN, "Player (X)");
        player_init(players + 1, GSQ_WHITE, get_difficulty(), "CPU (O)");
        return 0;
    }

    player_init(players + 0, GSQ_WHITE, PTYPE_HUMAN, "Player (O)");
    player_init(players + 1, GSQ_BLACK, get_difficulty(), "CPU (X)");
    return 1;
}

static int
settings_spectator(void)
{
    puts("CPU 1:");
    player_init(players + 0, GSQ_BLACK, get_difficulty(), "CPU 1 (X)");
    puts("CPU 2:");
    player_init(players + 1, GSQ_WHITE, get_difficulty(), "CPU 2 (O)");
    return 0;
}

/* selplayer: pointer to active player
 * return:    EXECTURN_*
 *
 * executes a turn of the game. 
 * EXECTURN_NEXT   advances play to the next player
 * EXECTURN_REPLAY forces the player to re-do their turn
 * EXECTURN_QUIT   quits the game
 *
 */
static int
execturn(struct player const *selplayer)
{
    char const *movetext;
    struct coord const *movecoord;

    draw_grid(selplayer->id);
    draw_score();

    if(grid_count_valid(selplayer->id) == 0)
        goto exit_forfeit;

    io_showprompt(selplayer->name);
    movetext = selplayer->movefn(selplayer->id);

    if(movetext == ai_addr_no_moves)
        goto exit_forfeit_bugreport;

    if(movetext == NULL)
        goto exit_eof;

    movecoord = coord_from_str(movetext);

    if(movecoord == NULL || !grid_exec_move(movecoord, selplayer->id))
        goto exit_badmove;

    printf("%s plays %s\n\n", selplayer->name, movetext);

    if(selplayer->movefn != human_move)
        io_waitcontinue();

    return EXECTURN_NEXT;

exit_badmove:
    puts("Move is Invalid or illegal. Try again.");
    return EXECTURN_REPLAY;

exit_forfeit:
    printf("%s has no valid moves and must forfeit their turn.\n", selplayer->name);
    return EXECTURN_NEXT;

exit_forfeit_bugreport:
    printf("%s has no valid moves and must forfeit their turn.\n", selplayer->name);
    puts("This state was reached in a very unusual way.");
    puts("Please go to https://gitlab.com/bradenbest/reversi/-/issues/new");
    puts("Title the issue 'reversi.c/execturn/exit_forfeit_bugreport'");
    puts("and include a screenshot of the last two moves and this information:");
    puts("* Game Type (singleplayer, multiplayer or spectator)");
    puts("* Player types for P1 and P2 (Human or CPU + diffculty)");
    puts("Returning to game.");
    return EXECTURN_NEXT;

exit_eof:
    puts("Received EOF. Terminating game.");
    return EXECTURN_QUIT;
}

static void
gameloop(void)
{
    int turnid = 0;
    int retval;

    if(players[0].id == GSQ_WHITE)
        ++turnid; // black plays first

    grid_init();

    while((retval = execturn(players + turnid)) != EXECTURN_QUIT){
        if(grid_count_valid(players[0].id) == 0 && grid_count_valid(players[1].id) == 0)
            return;

        if(retval == EXECTURN_NEXT)
            turnid = (turnid + 1) % 2;
    }
}

static void
endgamestats(void)
{
    int countblack = grid_count(GSQ_BLACK);
    int countwhite = grid_count(GSQ_WHITE);
    struct player *playerblack = players + 0;
    struct player *playerwhite = players + 1;

    if(playerblack->id == GSQ_WHITE){
        playerblack = players + 1;
        playerwhite = players + 0;
    }

    puts("Game Over.");
    draw_grid(GSQ_BLACK); // it doesn't matter who is passed in at this stage
    draw_score();

    if(countblack > countwhite)
        printf("%s Wins\n", playerblack->name);

    else if(countwhite > countblack)
        printf("%s Wins\n", playerwhite->name);

    else
        puts("Tie");
}

static void
player_init(struct player *selplayer, int pid, int ptype, char const *name)
{
    selplayer->id = pid;
    selplayer->movefn = movetextfns[ptype];
    memcpy(selplayer->name, name, min(31, strlen(name)));
}

static int
get_difficulty(void)
{
    puts("Select AI difficulty");
    puts("  1. Very Easy");
    puts("  2. Easy");
    puts("  3. Medium");
    puts("  4. Hard");
    puts("  5. Very Hard");
    io_showprompt("Settings/Select Difficulty");
    return clamp(io_getint() - 0, PTYPE_CPU_VEASY, PTYPE_END - 1);
}

static int
settings_gametype(void)
{
    puts("Choose game type");
    puts("  1. Multiplayer (Player v Player)");
    puts("  2. Singleplayer (Player v CPU)");
    puts("  3. Spectator (CPU v CPU)");
    io_showprompt("Settings/Game Type");
    return clamp(io_getint() - 1, GTYPE_HUMAN_HUMAN, GTYPE_END - 1);
}

static void
settings(void)
{
    int gametype = settings_gametype();
    int retval = settings_callbacks[gametype]();
    struct player *playerblack = players + retval;
    struct player *playerwhite = players + (1 - retval);

    printf("%s is Black\n", playerblack->name);
    printf("%s is White\n", playerwhite->name);
}
