#include <stdio.h>
#include <stdlib.h>

#define IO_C
#include "io.h"

/* buffer: string
 * bufsz:  size_t
 * return: number of bytes written to buffer
 *
 * Populates buffer with stdin until EOL, EOF, or capacity. Buffer is
 * not null-terminated. The following are discarded:
 *
 *   - newlines
 *   - excess content
 *   - lines ending in EOF
 */
size_t
io_getline(char *buffer, size_t bufsz)
{
    int ch;
    char *bufp = buffer;

    if(buffer == NULL)
        bufsz = 0;

    if(ferror(stdin) || feof(stdin))
        return 0;

    while((ch = getchar()) != EOF && ch != '\n')
        if((size_t)(bufp - buffer) < bufsz)
            *bufp++ = ch;

    if(ch == EOF)
        return 0;

    return bufp - buffer;
}

/* return: static char[3] (terminated) or NULL
 */
char const *
io_getmove(void)
{
    static char buffer[3];
    size_t nwritten = io_getline(buffer, 2);

    if(feof(stdin))
        return NULL;

    buffer[nwritten] = '\0';
    return buffer;
}

int
io_getint(void)
{
    char buffer[11] = "";

    io_getline(buffer, 10);
    return atoi(buffer);
}

void
io_waitcontinue(void)
{
    printf("Press Enter to continue.");
    io_getline(NULL, 0);
}

void
io_showprompt(char const *prompt)
{
    printf("%s >", prompt);
}
