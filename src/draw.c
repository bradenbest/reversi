#include <stdio.h>

#include "coord.h"
#include "grid.h"

#define DRAW_C
#include "draw.h"

void
draw_grid(int who)
{
    draw_grid_header_top();

    for(int row = 0; row < 8; ++row){
        draw_grid_header_side(row);

        for(int col = 0; col < 8; ++col)
            draw_grid_cell(who, col, row);

        draw_grid_header_side(row);
        putchar('\n');
    }

    draw_grid_header_top();
}

void
draw_score(void)
{
    printf("Scores: %u:%u\n", grid_count(GSQ_BLACK), grid_count(GSQ_WHITE));
}

// static

static char const * gridsquare_str[GSQ_END] = {
#ifdef DRAW_NOANSI
    GSQ_NONE_CHR,
#else
    "\e[35m" GSQ_NONE_CHR "\e[0m",
#endif
    GSQ_WHITE_CHR,
    GSQ_BLACK_CHR,
};

static char const * gridsquare_validmove =
#ifdef DRAW_NOANSI
    VALID_CHR
#else
    "\e[33m" VALID_CHR "\e[0m"
#endif
    ;

static void
draw_grid_cell(int who, int col, int row)
{
    struct coord const coord = {col, row};
    int cellvalue = grid_get_cell_from_coord(&coord);
    char const *cellstr = gridsquare_str[cellvalue];

    if(grid_is_move_valid(&coord, who))
        cellstr = gridsquare_validmove;

    printf("%s ", cellstr);
}
