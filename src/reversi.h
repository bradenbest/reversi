#ifndef REVERSI_H
#define REVERSI_H

#include "ai.h"

int main (void);

#endif // REVERSI_H



#ifdef REVERSI_C

typedef int (*settingsfn)(void);

enum playertype {
    PTYPE_HUMAN,
    PTYPE_CPU_VEASY,
    PTYPE_CPU_EASY,
    PTYPE_CPU_MEDIUM,
    PTYPE_CPU_HARD,
    PTYPE_CPU_VHARD,
    PTYPE_END
};

enum gametype {
    GTYPE_HUMAN_HUMAN,
    GTYPE_HUMAN_CPU,
    GTYPE_CPU_CPU,
    GTYPE_END
};

enum execturnret {
    EXECTURN_REPLAY,
    EXECTURN_NEXT,
    EXECTURN_QUIT,
};

struct player {
    char       name[32];
    int        id;
    movetextfn movefn;
};

static movetextfn    movetextfns        [PTYPE_END];
static settingsfn    settings_callbacks [3];
static struct player players            [2];

// movetextfn
static char const * human_move        (int unused);

// settingsfn
static int settings_multiplayer  (void);
static int settings_singleplayer (void);
static int settings_spectator    (void);

static int  execturn          (struct player const *selplayer);
static void gameloop          (void);
static void endgamestats      (void);
static void player_init       (struct player *selplayer, int pid, int ptype, char const *name);
static int  get_difficulty    (void);
static int  settings_gametype (void);
static void settings          (void);

#endif // REVERSI_C
