#ifndef UTIL_H
#define UTIL_H

int min   (int a, int b);
int clamp (int value, int mincap, int maxcap);

#endif // UTIL_H
