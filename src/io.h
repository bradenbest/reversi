#ifndef IO_H
#define IO_H

size_t       io_getline      (char *buffer, size_t bufsz);
char const * io_getmove      (void);
int          io_getint       (void);
void         io_waitcontinue (void);
void         io_showprompt   (char const *prompt);

#endif // IO_H



#ifdef IO_C

#endif // IO_C
