#include "util.h"

int
min(int a, int b)
{
    return a < b ? a : b;
}

int
clamp(int value, int mincap, int maxcap)
{
    if(value < mincap)
        return mincap;

    if(value > maxcap)
        return maxcap;

    return value;
}
