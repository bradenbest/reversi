#ifndef GRID_H
#define GRID_H

#include <stdint.h>

#include "coord.h"

#define grid_set_cell_from_str(str, value) \
    grid_set_cell(coord_1d_from_str(str), (value))

#define grid_get_cell_from_str(str) \
    grid_get_cell(coord_1d_from_str(str))

enum gridsquare {
    GSQ_NONE,
    GSQ_WHITE,
    GSQ_BLACK,
    GSQ_END
};

void        grid_init                (void);
uint8_t *   grid_get_offset          (int offset);
void        grid_set_cell            (int idx, int value);
int         grid_get_cell            (int idx);
void        grid_set_cell_from_coord (struct coord const *coord, int value);
int         grid_get_cell_from_coord (struct coord const *coord);
int const * grid_get_line            (struct coord const *origin, int dir);
void        grid_set_line            (struct coord const *origin, int dir, int who, int stop);
int         grid_set_line_dry        (struct coord const *origin, int dir, int who, int stop);
int         grid_count               (int value);
int         grid_count_line          (int const *line, int stopon);
int         grid_count_valid         (int who);
int         grid_is_move_valid       (struct coord const *coord, int who);
int         grid_exec_move           (struct coord const *coord, int who);
int         grid_exec_move_dry       (struct coord const *coord, int who);

#endif // GRID_H



#ifdef GRID_C

static uint8_t   grid       [64];
static int const directions [8];

static int check_line (int const *line, int who);

#endif // GRID_C
