#ifndef COORD_H
#define COORD_H

#define DIRFLAG_UPRIGHT   (DIRFLAG_UP | DIRFLAG_RIGHT)
#define DIRFLAG_DOWNRIGHT (DIRFLAG_DOWN | DIRFLAG_RIGHT)
#define DIRFLAG_DOWNLEFT  (DIRFLAG_DOWN | DIRFLAG_LEFT)
#define DIRFLAG_UPLEFT    (DIRFLAG_UP | DIRFLAG_LEFT)

struct coord {
    int col;
    int row;
};

enum directionflag {
    DIRFLAG_UP    = 0x1,
    DIRFLAG_RIGHT = 0x2,
    DIRFLAG_DOWN  = 0x4,
    DIRFLAG_LEFT  = 0x8,
};

struct coord const * coord_from_str       (char const *str);
struct coord const * coord_from_idx1d     (int idx1d);
int                  coord_1d_from_str    (char const *str);
int                  coord_1d_from_coord  (struct coord const *coord);
char const *         coord_str_from_1d    (int idx1d);
char const *         coord_str_from_coord (struct coord const *coord);
int                  coord_move           (struct coord *coord, int dir);

#endif // COORD_H



#ifdef COORD_C

static char const *coordcol_chr;

#endif // COORD_C
