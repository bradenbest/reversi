#ifndef DRAW_H
#define DRAW_H

void draw_grid  (int who);
void draw_score (void);

#endif // DRAW_H



#ifdef DRAW_C

#define draw_grid_header_top() \
    puts("  a b c d e f g h ")

#define draw_grid_header_side(rowid) \
    printf("%u ", (rowid) + 1)

#define GSQ_NONE_CHR "."
#define GSQ_WHITE_CHR "O"
#define GSQ_BLACK_CHR "X"
#define VALID_CHR "+"

static char const * gridsquare_str       [GSQ_END];
static char const * gridsquare_validmove          ;

static void draw_grid_cell (int who, int col, int row);

#endif // DRAW_C
