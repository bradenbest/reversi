#include <string.h>
#include <ctype.h>

#define COORD_C
#include "coord.h"

/* str:  string [a-h][1-8]
 * return: static {0-7, 0-7} or NULL
 */
struct coord const *
coord_from_str(char const *str)
{
    static struct coord out;
    char const *match = strchr(coordcol_chr, tolower(str[0]));
    int rowid = str[1] - '1';

    if(match == NULL || rowid < 0 || rowid > 7)
        return NULL;

    out.col = match - coordcol_chr;
    out.row = rowid;
    return &out;
}

/* idx1d:  0-63 1d grid index
 * return: static {0-7, 0-7} or NULL
 */
struct coord const *
coord_from_idx1d(int idx1d)
{
    static struct coord out;

    if(idx1d < 0 || idx1d > 63)
        return NULL;

    out.row = idx1d / 8;
    out.col = idx1d % 8;
    return &out;
}

/* str:    string [a-h][1-8]
 * return: 0..63 or -1
 */
int
coord_1d_from_str(char const *str)
{
    return coord_1d_from_coord(coord_from_str(str));
}

/* coord:  {0-7, 0-7}
 * return: 0..63 or -1
 */
int
coord_1d_from_coord(struct coord const *coord)
{
    if(coord == NULL)
        return -1;

    return 8 * coord->row + coord->col;
}

char const *
coord_str_from_1d(int idx1d)
{
    static char buffer[3];

    if(idx1d < 0 || idx1d > 63)
        return NULL;

    buffer[0] = 'a' + (idx1d % 8);
    buffer[1] = '1' + (idx1d / 8);
    return buffer;
}

char const *
coord_str_from_coord(struct coord const *coord)
{
    return coord_str_from_1d(coord_1d_from_coord(coord));
}

/* coord:  coordinate to modify
 * dir:    direction to move in
 * return: 1 or 0
 *
 * makes a copy of the coordinate, moves it, and checks if it's out of
 * bounds. If so, then 0 is returned for failure and the coordinate
 * remains unmodified. Otherwise, the buffer is copied back and 1 is
 * returned.
 */
int
coord_move(struct coord *coord, int dir)
{
    struct coord temp;

    memcpy(&temp, coord, sizeof *coord);

    if(dir & DIRFLAG_UP)
        --temp.row;

    if(dir & DIRFLAG_RIGHT)
        ++temp.col;

    if(dir & DIRFLAG_DOWN)
        ++temp.row;

    if(dir & DIRFLAG_LEFT)
        --temp.col;

    if(temp.col < 0 || temp.col > 7 || temp.row < 0 || temp.row > 7)
        return 0;

    memcpy(coord, &temp, sizeof temp);
    return 1;
}

// static

static char const *coordcol_chr = "abcdefgh";
