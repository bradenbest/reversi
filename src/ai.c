#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "coord.h"
#include "grid.h"

#define AI_C
#include "ai.h"

char const *ai_addr_no_moves = "??";

/* input callbacks
 * who: GSQ_BLACK or GSQ_WHITE
 * return: string [a-h][1-8]
 *
 * these functions take a player ID (GSQ_BLACK or GSQ_WHITE) as input
 * and return a string representing a move such as "d3"
 */

char const *
ai_cpu_veryeasy_move(int who)
{
    struct aimovemap const *moves = movelist_sorted(get_movelist(who), cmpfn_movelist_asc);

    if(movelist_len(moves) == 0)
        return ai_addr_no_moves;

    return moves->movetext;
}

char const *
ai_cpu_easy_move(int who)
{
    return ai_cpu_generic_rand3(who, cmpfn_movelist_asc);
}

char const *
ai_cpu_medium_move(int who)
{
    struct aimovemap const *moves = get_movelist(who);
    size_t len = movelist_len(moves);

    if(len == 0)
        return ai_addr_no_moves;

    return (moves + (rand() % len))->movetext;
}

char const *
ai_cpu_hard_move(int who)
{
    return ai_cpu_generic_rand3(who, cmpfn_movelist_desc);
}

char const *
ai_cpu_veryhard_move(int who)
{
    struct aimovemap const *moves = movelist_sorted(get_movelist(who), cmpfn_movelist_desc);
    size_t len = movelist_len(moves);
    static char const *corners[] = { "a1", "h1", "a8", "h8" };

    if(len == 0)
        return ai_addr_no_moves;

    for(int i = 0; i < 4; ++i)
        if(grid_is_move_valid(coord_from_str(corners[i]), who))
            return corners[i];

    return moves->movetext;
}

// static

static struct aimovemap const *
get_movelist(int who)
{
    static struct aimovemap moves[64];
    struct aimovemap *selmove = moves;

    for(int i = 0; i < 64; ++i){
        if(grid_is_move_valid(coord_from_idx1d(i), who)){
            memcpy(selmove->movetext, coord_str_from_1d(i), 3);
            selmove->score = grid_exec_move_dry(coord_from_idx1d(i), who);
            ++selmove;
        }
    }

    selmove->score = -1;
    return moves;
}

static size_t
movelist_len(struct aimovemap const *movemap)
{
    size_t len = 0;

    if(movemap == NULL)
        return 0;

    while(len < 64 && movemap[len].score != -1)
        ++len;

    return len;
}

static struct aimovemap const *
movelist_sorted(struct aimovemap const *movemap, cmpfn callback)
{
    static struct aimovemap moves_cp[64];
    size_t len = movelist_len(movemap);

    if(len == 0)
        return NULL;

    memcpy(moves_cp, movemap, (1 + len) * sizeof *movemap);
    qsort(moves_cp, len, sizeof *moves_cp, callback);
    return moves_cp;
}

static char const *
ai_cpu_generic_rand3(int who, cmpfn sorter)
{
    struct aimovemap const *moves = movelist_sorted(get_movelist(who), sorter);
    size_t len = movelist_len(moves);

    if(len == 0)
        return ai_addr_no_moves;

    return (moves + (rand() % min(len, 3)))->movetext;
}

static int
cmpfn_movelist_asc(void const *a, void const *b)
{
    struct aimovemap const *value_a = a;
    struct aimovemap const *value_b = b;

    return value_a->score - value_b->score;
}

static int
cmpfn_movelist_desc(void const *a, void const *b)
{
    struct aimovemap const *value_a = a;
    struct aimovemap const *value_b = b;

    return value_b->score - value_a->score;
}
