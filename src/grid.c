#include <stdio.h>
#include <string.h>

#include "util.h"
#include "coord.h"

#define GRID_C
#include "grid.h"

/* Sets the center squares to their appropriate starting values
 */
void
grid_init(void)
{
    grid_set_cell_from_str("d4", GSQ_WHITE);
    grid_set_cell_from_str("e5", GSQ_WHITE);
    grid_set_cell_from_str("e4", GSQ_BLACK);
    grid_set_cell_from_str("d5", GSQ_BLACK);
}

/* offset: 0-63
 * return: pointer to offset in grid
 */
uint8_t *
grid_get_offset(int offset)
{
    return grid + offset;
}

/* idx:   0-63
 * value: GSQ_*
 */
void
grid_set_cell(int idx, int value)
{
    grid[idx] = value;
}

/* idx:    0..63
 * return: GSQ_*
 *     valid   -> value at index
 *     invalid -> GSQ_END
 */
int
grid_get_cell(int idx)
{
    if(idx < 0 || idx > 63)
        return GSQ_END;

    return grid[idx];
}

/* coord: {0-7, 0-7}
 * value: GSQ_*
 */
void
grid_set_cell_from_coord(struct coord const *coord, int value)
{
    grid_set_cell(8 * coord->row + coord->col, value);
}

/* coord:  {0-7, 0-7}
 * return: GSQ_*
 *     valid   -> value at coordinate
 *     invalid -> GSQ_END
 */
int
grid_get_cell_from_coord(struct coord const *coord)
{
    if(coord->col < 0 || coord->col > 7 || coord->row < 0 || coord->row > 7)
        return GSQ_END;

    return grid_get_cell(8 * coord->row + coord->col);
}

/* origin:  {0-7, 0-7} line origin coordinate
 * dir:     DIRFLAG_* direction of line
 * return:  static int[8] terminated by GSQ_END
 *
 * draws a line from the origin to the edge of the grid containing the
 * values of each cell (except the cell at origin)
 */
int const *
grid_get_line(struct coord const *origin, int dir)
{
    struct coord temp;
    static int buffer[8];
    int *bufp = buffer;

    memcpy(&temp, origin, sizeof *origin);

    while(coord_move(&temp, dir))
        *bufp++ = grid_get_cell_from_coord(&temp);

    *bufp++ = GSQ_END;
    return buffer;
}

/* origin: {0-7, 0-7} line origin coordinate
 * dir:    DIRFLAG_*  direction of line
 * who:    GSQ_BLACK or GSQ_WHITE
 * stop:   iteration to stop at
 *
 * draws a line from the origin to the edge of the grid and sets all the
 * values to who. Similar to grid_get_line, the origin is excluded from
 * the operation. The stop argument is there to short the line, as in a
 * move, a line isn't supposed to cross the entire grid
 */
void
grid_set_line(struct coord const *origin, int dir, int who, int stop)
{
    struct coord temp;

    memcpy(&temp, origin, sizeof *origin);

    for(int i = 0; i < stop && coord_move(&temp, dir); ++i)
        grid_set_cell_from_coord(&temp, who);
}

/* same as grid_set_line except it has no side effects and returns the
 * number of opponent squares on the line. This is used for the AI
 * player
 */
int
grid_set_line_dry(struct coord const *origin, int dir, int who, int stop)
{
    struct coord temp;
    int total = 0;

    memcpy(&temp, origin, sizeof *origin);

    for(int i = 0; i < stop && coord_move(&temp, dir); ++i)
        if(grid_get_cell_from_coord(&temp) != who)
            ++total;

    return total;
}

/* value:  GSQ_* value to count
 * return: number of occurrences of value
 */
int
grid_count(int value)
{
    int total = 0;

    for(int i = 0; i < 64; ++i)
        if(grid[i] == value)
            ++total;

    return total;

}

/* line:   int[8] from grid_get_line
 * stopon: value to stop on
 * return: length of line
 */
int
grid_count_line(int const *line, int stopon)
{
    int total = 0;

    for(int i = 0; line[i] != GSQ_END && line[i] != stopon; ++i)
        ++total;

    return total;
}


/* who:    GSQ_BLACK or GSQ_WHITE
 * return: number of possible valid moves
 *
 * counts every cell for given player where it is a valid move
 */
int
grid_count_valid(int who)
{
    struct coord const *coord;
    int total = 0;

    if(who != GSQ_WHITE && who != GSQ_BLACK)
        return 0;

    for(int i = 0; i < 64; ++i){
        coord = coord_from_idx1d(i);

        if(grid_is_move_valid(coord, who))
            ++total;
    }

    return total;
}

/* coord:  {0-7, 0-7} coordinate to check
 * who:    GSQ_BLACK or GSQ_WHITE
 * return: 1 or 0
 *
 * A move is valid if an uninterrupted line can be drawn from the
 * destination in one of 8 directions to a cell belonging to who.
 * "uninterrupted" meaning none of the cells are empty.
 *
 * Also the coord cannot already be owned.
 */
int
grid_is_move_valid(struct coord const *coord, int who)
{
    int value_at_coord = grid_get_cell_from_coord(coord);

    if(value_at_coord == GSQ_WHITE || value_at_coord == GSQ_BLACK)
        return 0;

    for(int i = 0; i < 8; ++i)
        if(check_line(grid_get_line(coord, directions[i]), who))
            return 1;

    return 0;
}

/* coord:  {0-7, 0-7} coordinate to execute move on
 * who:    GSQ_WHITE or GSQ_BLACK
 * return: 1 or 0
 *
 * executes move by checking if the move is valid, setting cell at coord
 * to who, and calling grid_set_line in each direction
 * assumes grid_mark_valid was called beforehand
 */
int
grid_exec_move(struct coord const *coord, int who)
{
    if(!grid_is_move_valid(coord, who))
        return 0;

    grid_set_cell_from_coord(coord, who);

    for(int i = 0; i < 8; ++i){
        int const *line = grid_get_line(coord, directions[i]);
        int linelen = grid_count_line(line, GSQ_NONE);

        if(check_line(line, who))
            grid_set_line(coord, directions[i], who, linelen);
    }

    return 1;
}

/* same as grid_exec_move, except it has zero side effects and instead
 * counts and returns the number of opponent squares that will be
 * flipped by the move. This is used for the AI player.
 */
int
grid_exec_move_dry(struct coord const *coord, int who)
{
    int total = 0;

    if(!grid_is_move_valid(coord, who))
        return 0;

    ++total;

    for(int i = 0; i < 8; ++i){
        int const *line = grid_get_line(coord, directions[i]);
        int linelen = grid_count_line(line, GSQ_NONE);

        if(check_line(line, who))
            total += grid_set_line_dry(coord, directions[i], who, linelen);
    }

    return total;
}

// static

static uint8_t grid[64];

static int const directions[8] = {
    DIRFLAG_UP,
    DIRFLAG_UPRIGHT,
    DIRFLAG_RIGHT,
    DIRFLAG_DOWNRIGHT,
    DIRFLAG_DOWN,
    DIRFLAG_DOWNLEFT,
    DIRFLAG_LEFT,
    DIRFLAG_UPLEFT,
};

static int
check_line(int const *line, int who)
{
    int opponent = GSQ_WHITE;

    if(who == GSQ_WHITE)
        opponent = GSQ_BLACK;

    if(line[0] != opponent)
        return 0;

    for(int i = 1; line[i] != GSQ_END; ++i){
        if(line[i] == GSQ_NONE)
            return 0;

        if(line[i] == who)
            return 1;
    }

    return 0;
}
