# reversi

Simple Reversi implementation in C by Braden Best

This has three game modes: singleplayer (human v human), multiplayer (human v cpu) and spectator (cpu v cpu).

## Build

    $ make
    or
    $ make release-noansi

The `release-noansi` target builds without ANSI escape sequences.
